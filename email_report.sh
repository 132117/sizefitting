#!/bin/bash

# some variables
# refactoring the script such that all these values are
# passed from the outside as arguments should be easy


from=$1
to=$2
subject="Size Fitting Update Report `date`"
echo $subject
boundary="ZZ_/afg6432dfgkl.94531q"
body=`cat job_log.txt | grep '*'`

# Build headers
{

printf '%s\n' "From: $from
To: $to
Subject: $subject
Mime-Version: 1.0
Content-Type: multipart/mixed; boundary=\"$boundary\"

--${boundary}
Content-Type: text/plain; charset=\"US-ASCII\"
Content-Transfer-Encoding: 7bit
Content-Disposition: inline

$body
"
} | /usr/sbin/sendmail $to   # one may also use -f here to set the envelope-from
