import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
from io import StringIO
parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store


#remove fits from product
def remove_fits_prod(i,id):#i is product data dictionary, caregory_id is id string
    updated_product=i#initiate the update body from the product data
    cut_description=updated_product['description'].split('<div class="car-fits">')#keep the part before div for fits list
    cut_description[1]=cut_description[1].split('</div>',1)[1]#keep the part after div for fits list
    new_description=''.join(cut_description)
    updated_product['description']=new_description#assign new description
    product_update=json.dumps(updated_product,ensure_ascii=True)#turn python dictionary into a json for request
    r = requests.put(base_url+'catalog/products/%s'%i['id'], data=product_update,headers=headers)#submit update request
    print 'Product ID/SKU',i['id'],'/',i['sku'],'fits removed %s'%(str(r.status_code))#prints status for appending the list to description

#add fits to product
def add_fits_prod(i,id,fits_list,size):#i is product data dictionary, caregory_id is id string
    if len(fits_list)>0 and '<div class="car-fits">' not in i['description']:
        updated_product=i#initiate the update body from the product data
        updated_product['description']=updated_product['description']+'<div class="car-fits"><p>Below is a list of some popular vehicles that %s tires may fit depending on Year &amp; Option. However the list does not cover all vehicle\'s that these tires can fit:</p><ul><li>'%size+fits_list+'</li></ul></div>'#add fits list to description with div class markers [WILL CHANGE THIS PART AS NECESSARY]
        product_update=json.dumps(updated_product,ensure_ascii=True)#turn python dictionary into a json for request
        r = requests.put(base_url+'catalog/products/%s'%i['id'], data=product_update,headers=headers)#submit update request
        print 'Product ID/SKU',i['id'],'/',i['sku'],'fits added to description %s'%(str(r.status_code))#prints status for appending the list to description

#remove fits from category
def remove_fits_cat(i,id):#i is product data dictionary, caregory_id is id string
    updated_category=i#initiate the update body from the product data
    cut_description=updated_category['description'].split('<div class="prolist_by_size_desc">')#keep the part before div for fits list
    cut_description[1]=cut_description[1].split('</div>',1)[1]#keep the part after div for fits list
    new_description=''.join(cut_description)
    updated_category['description']=new_description#assign new description
    category_update=json.dumps(updated_category,ensure_ascii=True)#turn python dictionary into a json for request
    r = requests.put(base_url+'catalog/categories/%s'%i['id'], data=category_update,headers=headers)#submit update request
    print 'Category ID/Name',i['id'],'/',i['name'],'fits removed %s'%(str(r.status_code))#prints status for appending the list to description



def add_fits_cat(i,id,fits_list):#i is product data dictionary, caregory_id is id string
    updated_category=i#initiate the update body from the product data
    updated_category['description']=updated_category['description']+'<div class="prolist_by_size_desc"><p>Below is a list of some popular vehicles that %s tires may fit depending on Year &amp; Option:</p><ul><li>'%i['name']+fits_list+'</li></ul></div>'#add fits list to description with div class markers [WILL CHANGE THIS PART AS NECESSARY]
    category_update=json.dumps(updated_category,ensure_ascii=True)#turn python dictionary into a json for request
    r = requests.put(base_url+'catalog/categories/%s'%i['id'], data=category_update,headers=headers)#submit update request
    print 'Category ID/Name',i['id'],'/',i['name'],'fits added to description %s'%(str(r.status_code))#prints status for appending the list to description

start_time=datetime.now()

args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash#set up the base url for api access
product_data=[]#initiate storage for all products

r = requests.get(base_url+'catalog/products',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
print '************ Size Fitting Relisting Report',start_time,'***********'#logging the beginning of the run
print 'Product count request',r.status_code#initital request fulfillment report
#marking beginning of the update cycle
data=json.loads(r.text)#turn the json response into a python dictionary
product_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = requests.get(base_url+'catalog/products?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print page,"of",page_count,"page responses",r.status_code#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    product_data=product_data+data

print 'Category count request'
r = requests.get(base_url+'catalog/categories',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
data=json.loads(r.text)#turn the json response into a python dictionary
category_data=[]
category_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = requests.get(base_url+'catalog/categories?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print page,"of",page_count,"page responses",r.status_code#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    category_data=category_data+data

category_dataframe=pd.DataFrame(data=category_data)#create dataframe of products and categories from dicts
size_id=str(category_dataframe[category_dataframe['name']=='By Size']['id'].iloc[0])#get the category id
product_dataframe=pd.DataFrame(data=product_data)
product_dataframe=product_dataframe.set_index('id')#use id's to look up products
fittings_url='https://www.prioritytireoutlet.com/content/tgpSize_1801_US.csv'#url to download fitting data
fittings_data=pd.read_csv(StringIO(requests.get(fittings_url).text))#create dataFrame for table
#size_id - category parent by_size
size_id_table=category_dataframe[category_dataframe['parent_id'].apply(lambda x: str(x))==size_id][['id','name']].drop_duplicates()#only keep size category ids and their names
id_car_table=size_id_table.merge(fittings_data,how='left',left_on='name',right_on='RAWSIZE')#assign size category id's to corresponding RAWSIZEs
id_car_table=id_car_table[['MAKE2','MODEL','OPTION2','id','RAWSIZE']].drop_duplicates()#keep all correspinding data and id's
id_car_table['Car']=id_car_table['MAKE2']+' '+id_car_table['MODEL']+' '+id_car_table['OPTION2']#make a string for a car description
id_car_table=id_car_table[id_car_table['Car'].notnull()]
id_car_table['Car']=id_car_table['Car'].apply(lambda x: x.replace(' Base','') if x[-5:]==' Base' else x)#remove base trims

removal_count=0
addition_count=0
for i in product_data:#cycle through products
    car_list=id_car_table[id_car_table['id'].isin(product_dataframe['categories'].loc[i['id']])]
    list_str=car_list['Car'].sort_values(ascending=True).str.cat(sep='</li><li>')#combine the column of cars
    if '<div class="car-fits">' in i['description'] and ('</p><ul><li>' in i['description']):#check if car_list is already there
        existing_list=i['description'].split('</p><ul><li>',1)[1]
        existing_list=existing_list.split('</li></ul></div>',1)[0]#check what it looks like
        if list_str!=existing_list:#if it is different then remove it, to add a new one with another function
            remove_fits_prod(i,i['id'])
            removal_count+=1
    if len(list_str)>1 and '<div class="car-fits">' not in i['description']:#if there is no list of car fits, but there are cars that fit then add it, condition is >1 to avoid '-'
        add_fits_prod(i,i['id'],list_str,car_list['RAWSIZE'].iloc[0])
        addition_count+=1

print '***** Product: %d fit lists removed and %d fit lists added'%(removal_count,addition_count)


removal_count=0
addition_count=0
for i in category_data:#cycle through categories
    car_list=id_car_table[id_car_table['id']==i['id']]['Car'].sort_values(ascending=True).str.cat(sep='</li><li>')#combine the column of cars relevant to the product into one string
    if ('<div class="prolist_by_size_desc">' in i['description']) and ('</p><ul><li>' in i['description']):#check if category-cars list is already in the description
        existing_list=i['description'].split('<\p><ul><li>',1)[1]
        existing_list=existing_list.split('</li></ul></div>',1)[0]#check what it looks like
        if car_list!=existing_list:#if it is different then remove it, to add a new one with another function
            remove_fits_cat(i,i['id'])
            removal_count+=1
    if len(car_list)>1 and '<div class="prolist_by_size_desc">' not in i['description']:#if there is no list of car fits, but there are cars that fit then add it, condition is >1 to avoid '-'
        add_fits_cat(i,i['id'],car_list)
        addition_count+=1
print '***** Category: %d fit lists removed and %d fit lists added'%(removal_count,addition_count)
print '******** Finished after',start_time-datetime.now(),'**********\n\n\n'


