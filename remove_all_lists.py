import requests
import json
from math import ceil
from datetime import datetime
import argparse
import pandas as pd
from io import StringIO
parser = argparse.ArgumentParser(description='Update Clearance Category on Products')
parser.add_argument('--clientId', help='Client ID for headers')
parser.add_argument('--APIToken', help='Access token')#setup arguments for the script
parser.add_argument('--StoreHash', help='Store hash')#supply the hash to reach the store


#remove fits from product
def remove_fits_prod(i,id):#i is product data dictionary, caregory_id is id string
    updated_product=i#initiate the update body from the product data
    while '<div class="car-fits">' in updated_product['description']:
        cut_description=updated_product['description'].split('<div class="car-fits">')#keep the part before div for fits list
        cut_description[1]=cut_description[1].split('</div>',1)[1]#keep the part after div for fits list
        new_description=''.join(cut_description)
        updated_product['description']=new_description#assign new description
    product_update=json.dumps(updated_product,ensure_ascii=True)#turn python dictionary into a json for request
    r = requests.put(base_url+'catalog/products/%s'%i['id'], data=product_update,headers=headers)#submit update request
    print 'Product ID/SKU',i['id'],'/',i['sku'],'fits removed %s'%(str(r.status_code))#prints status for appending the list to description

#remove fits from category
def remove_fits_cat(i,id):#i is product data dictionary, caregory_id is id string
    updated_category=i#initiate the update body from the product data
    while '<div class="prolist_by_size_desc">' in updated_category['description']:
        cut_description=updated_category['description'].split('<div class="prolist_by_size_desc">')#keep the part before div for fits list
        cut_description[1]=cut_description[1].split('</div>',1)[1]#keep the part after div for fits list
        new_description=''.join(cut_description)
        updated_category['description']=new_description#assign new description
    category_update=json.dumps(updated_category,ensure_ascii=True)#turn python dictionary into a json for request
    r = requests.put(base_url+'catalog/categories/%s'%i['id'], data=category_update,headers=headers)#submit update request
    print 'Category ID/Name',i['id'],'/',i['name'],'fits removed %s'%(str(r.status_code))#prints status for appending the list to description



start_time=datetime.now()

args = parser.parse_args()
headers={"Accept": "application/json","Content-Type": "application/json","X-Auth-Client": args.clientId,"X-Auth-Token": args.APIToken}#authorization with bigcommerce
base_url='https://api.bigcommerce.com/stores/%s/v3/'%args.StoreHash#set up the base url for api access
product_data=[]#initiate storage for all products

r = requests.get(base_url+'catalog/products',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
print '************ Size List Removal Report',start_time,'***********'#logging the beginning of the run
print 'Product count request',r.status_code#initital request fulfillment report
#marking beginning of the update cycle
data=json.loads(r.text)#turn the json response into a python dictionary
product_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = requests.get(base_url+'catalog/products?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print page,"of",page_count,"page responses",r.status_code#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    product_data=product_data+data

print 'Category count request'
r = requests.get(base_url+'catalog/categories',headers=headers)#call to big commerce to get the amount of products, in order to know how many pages to go through
data=json.loads(r.text)#turn the json response into a python dictionary
category_data=[]
category_count=int(data['meta']['pagination']['total'])#see how many products there are
page_count=int(ceil(product_count/250.0))#calculate the number of pages to go through by displaying 250 items on each page
for page in range(1,page_count+1):
    r = requests.get(base_url+'catalog/categories?limit=250&page=%d'%page,headers=headers)#get the data on products on given page
    print page,"of",page_count,"page responses",r.status_code#page request status
    data=json.loads(r.text)['data']#turn json response into python dictionary
    category_data=category_data+data

removal_count=0
for i in product_data:
    if '<div class="car-fits">' in i['description']:
        remove_fits_prod(i,i['id'])
        removal_count+=1
removal_count=0

for i in category_data:
    if '<div class="prolist_by_size_desc">' in i['description']:
        remove_fits_cat(i,i['id'])
        removal_count+=1
print '***** Product: %d fit lists removed'%(removal_count)
print '***** Category: %d fit lists removed'%(removal_count)
print '******** Finished after',start_time-datetime.now(),'**********\n\n\n'
